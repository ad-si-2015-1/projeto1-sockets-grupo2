Passos e metodologia adotada para Implementação
===================


Este documento faz referência a forma como nós do grupo 2 adotamos para implementarmos nossos agentes (classes, métodos e formas de comunicação). Entre outros itens, utilizamos como ferramenta <i class="icon-cog"></i> a IDE Eclipse como base e a todo momento seguindo um roteiro pré estabelecido em outros documentos, fluxogramas e principalmente em ***Issues*** fixadas pelo líder do grupo (@brunokarpo) e os demais integrantes.

----------


Iniciando Projeto
-----------------

Com o Eclipse baixado e instalado na sua máquina é hora de começar a desenvolver. Antes de tudo faça o clone do repositório e crie sua branch. Com sua branch criada abra o Eclipse:

- Clique em *File* no canto superior esquerdo da tela e em seguida em *Import*;
- Pesquise por *Maven* e em seguida selecione *Existing Maven Projetcs*;

![enter image description here](https://lh3.googleusercontent.com/-KURkgCvvRt0/VRhwgLVe2aI/AAAAAAAABOE/pNftN7Cif54/s0/1.png "Eclipse - Criando Projeto")

- Clique em *Next*;
- Na próxima tela você deve apontar para a pasta com o código fonte da aplicação:

![enter image description here](https://lh3.googleusercontent.com/-gmlIOcAuhYg/VRhxVinVRHI/AAAAAAAABOY/vlc6Z-pW_AM/s0/2.png "POM")

- Selecione a pasta que contém o código fonte e clique em *Finish*. Projeto importado!

#### Agente Base

O primeiro agente que temos de conhecer na aplicação é a ***classe* Agente**. Ele é o agente base de toda nossa implementação. Esse agente é abstrato, ou seja, não e possível fazer algo do tipo:
```javascript
Agente obj = new Agente();
```
Todos os demais agentes implementados deverão herdar da classe Agente, pois ela contém a implementação base para todos os agentes do jogo. Os atributos do agente base são:

1. Constantes Estásticas:
    * **PORTA_MINIMA**: informa a menor porta que o agente pode assumir ao ser executado;
    * **PORTA_MAXIMA**: informa a maior porta que o agente pode assumir ao ser executado;
2. Atributos do Agente:
    * **endereco**: guarda o endereço da máquina onde o agente foi instanciado;
    * **porta**: guarda o número da porta onde o agente está ouvindo as conexões;
    * **ativo**: atributo que informa se o agente está ativo, ou seja, *está executando as ações que ele foi programado para efetuar*. Inicialmente recebe o valor false pois o agente só é ativado quando recebe o primeiro contato da Aranha.
3. Atributos de identificação da Aranha: 

> Esses atríbutos existem para os agentes saberem onde está a aranha e como contactar ela.

* **enderecoAranha**: guarda o IP da Aranha em uma String;
* **portaAranha**: guarda a porta onde a Aranha ouve as conexões.
4. Também foi criado um atributo para guardar o Socket Servidor:
* **serverSocket**: Atributo que vai ser iniciado junto com o Agente. Esse Socket é responsável por receber as conexões dos agentes externos que interagem com esse agente;

Qualquer agente ao ser iniciado chama o construtor padrão da classe abstrata *Agente*. Ele foi definido da seguinte forma: 

```javascript
Agente() {
while(serverSocket == null && porta!= PORTA_MAXIMA){
	try{
		serverSocket = new ServerSocket(porta);
	} catch(IOException e){
		System.out.println("Porta " + porta + "Indisponível para uso");
		}
	}
	setEndereco(serverSocket.getInetAddress());
		
```

**O que foi feito?**  Nosso agente valida se o serverSocket já foi instanciado e se o atributo `porta` é diferente da constante `PORTA_MAXIMA`. Se os testes forem verdadeiros, ele tenta instanciar o serverSocket na porta. Caso a porta esteja ocupada ele vai para a próxima porta e tenta instanciar. Isso será feito até ele achar uma porta disponível entre a *14440* e a *14800*.

Se ele conseguir, ele pega o endereço de IP do host onde o agente foi instanciado e seta no seu atributo endereço. Os agentes também possuem dois outros métodos, um chamado **ouvir()** e outro chamado **agir()**. Esses métodos tem a função de iniciar o ciclo de aguardo de mensagens de um agente e o seu ciclo de ação. Métodos estes que tal como o construtor são básicos para todos os agentes e já está pronto.

#### Método Ouvir
O método ouvir inicia o ciclo de escuta de um agente. Esse ciclo é o que vai possibilitar o agente receber interações dos outros agentes da rede. Ele foi implementado da seguinte forma:

```javascript
void ouvir() throws IOException{
	while (serverSocket != null){
		System.out.printl("Aguardando conexão do próximo agente...");
		Socket socket = serverSocket.accpet();

		acaoEscutarImp(socket);
	}
	serverSocket.close();
}
		
```

Como o construtor já havia criado o serverSocket, agora enquanto esse serverSocket estiver ativo o agente vai ficar aguardando uma conexão. Quando uma conexão é obtida, o método ouvir chama o método acaoEscutarImp() passando como parâmetro o socket obtido pela conexão; Método **acaoEscutarImp()** explicado logo abaixo.

#### Método Agir
O método agir inicia o ciclo de ação de um agente. Esse ciclo é o que possibilita o agente interagir com outros agentes presentes na rede. Ele foi implementado da seguinte forma:

```javascript
void agir() 
	while (serverSocket != null){
		System.out.printl("Iniciando atividade do agente");
		if(ativo){
			acaoAgirImp();
		}else{
				try{
					Thread.sleep(5000);
				}catch (InterruptedException e){
					e.printStackTrace();
					}
				}
			}
		}
		
```
Um agente só pode agir se ele estiver escutando, portanto enquanto o serverSocket estiver ativo ele vai tentar iniciar uma ação do agente. Porém existe uma estrutura condicional ali que testa se o agente está ativo. Por quê? A primeira coisa que todo agente deve fazer antes de executar alguma ação é buscar com a Aranha um agente com que ele possa interagir, mas o agente, ao ser iniciado não conhece onde a Aranha está na rede. Esse é o motivo de o atributo ativo iniciar como `false`. Ele fica ouvindo conexão até a Aranha encontrar ele. Quando a Aranha o encontra, ele pega o IP e a PORTA da Aranha. Então o agente deve mudar o agir para `true` o que vai iniciar seu ciclo de ação. Se a Aranha ainda não o encontrou, ele vai aguardar 5 segundos antes de validar se ele já foi ativado, ou seja, encontrado por uma **Aranha**.

#### Método acaoEscutarImp
Esse é um método abstrato, ou seja, qualquer agente que herde de Agente vai ter de implementar esse método. É aqui que você deve informar como seu agente deve agir de acordo com as mensagens que ele receber. A implementação base desse agente deve seguir o exemplo do agente Aranha:

```javascript
void acaoEscutarImp(Socket socket){
	try{
		Envelope recebida = SocketUtils.aguardarMensagem(socket);
		Envelope resposta;

		switch(recebida.getMensagem()){
		case IDENTIFICAR:
			break;
		case INFORME_UM_AGENTE:
			break;
		case SOU_ARANHA:
			break;
		default:
			System.out.println("Aranha não responde a esse comando");
			}
		}
	}
		
```

Essa é a implemtação básica: inicialmente você recebe um **Envelope** com a mensagem de outro agente que mandou algo para você. Você então cria um objeto **Envelope** onde estará sua resposta. Pega a mensagem que recebeu e testa qual foi a mensagem passada ali e executa alguma ação sobre essa informação.

##### Até esse ponto já falamos termos como Thread, Envelope e Mensagens. O que são?

> A classe **Envelope**
	Dentro do projeto existe um pacote chamado br.ufg.si.ad.grupo2.utils. Esse pacote tem algumas coisas criadas para facilitar a vida de quem está implementando o projeto e uma dessas coisas é a classe Envelope. A classe Envelope encapsula as *mensagens* que serão trocadas entre os agentes, definindo um padrão de comunicação e o que os agentes precisam enviar em suas comunicações. Essa classe tem os seguintes atributos: 
	
>	- **ip** : é uma String que deve conter o endereço de IP que se deseja informar na comunicação. Ele depende de cada agente. Existem agentes que vão enviar seu próprio IP nessa String, tal como agente Aranha ao solicitar uma identificação, ou então um IP do alvo, tal como o agente Aranha ao informar um IP de um certo agente para um outro agente;
    
>   - **porta** : é a mesma coisa do IP. Se a mensagem contém um IP, ele deve conter uma porta para permitir a conexão, pode ser a porta de quem está enviando ou a porta de um alvo, dependendo da necessidade;
    
>   - **instanciaAgente** : aqui será informado a instância do agente da comunicação. Pode ser uma comunicação do tipo “sou o agente instanciaAgente” ou “sou o agente instanciaAgente”;
    
>   - **mensagem**: é um enum do tipo  MENSAGENS_AGENTE. Esse enum (vou explicar mais à frente) contém as mensagens que serão trocadas na aplicação. Todas as mensagens devem estar padronizadas aqui, para possibilitar qualquer agente recebê-la, interpretá-la e respondê-la. Nesse atributo será enviado o comando ou a resposta a um comando dentro de uma comunicação entre os agentes;
    
>   - **mensagemPersonalizada**: esse atributo foi criado especialmente para o questionador e respondendor, justamente para enviar e receber desafios aritméticos. Os demais agentes devem enviar NULL nesse campo.

Alguns detalhes importantes dessa classe é que ela só é possível de ser construída através de um construtor que recebe como parametro TODAS as informações de uma comunicação. Você consegue ler as informações (métodos `GETs`), porém uma vez construído um objeto mensagem, não é possível alterar suas informações (não existe métodos `SET`).

> Enum **MENSAGENS_AGENTES**
Imagina um mundo onde não existe um padrão de linguagem. Em programação esse é um problema que perturba a vida dos programadores quando uma classe manda uma String para outra classe interpretar e agir conforme essa String. Se um ponto, um espaço, um acento é passado indevidamente o caos está formado. É justamente para evitar que esse problema ocorra que técnicas como definição de ENUMs são utilizadas. Nesse cenário você coloca em um classe de enumeração todos os comandos que devem ser enviados / interpretados. O Enum MENSAGENS_AGENTES serve basicamente como um dicionário que mostra todas as sentenças que os agentes devem conhecer / implementar.

```javascript
public enum MENSAGENS_AGENTES {
																														
	//Mensagens de contaminação
	SOU_IMUNE("Não é possível me contaminar"),																				
	FUI_MORDIDO("Fui mordido e agora quero um cerebro"), 															    							
	INFORME_UM_AGENTE("me informe um agente"),																			   		 						

	//Mensagens enviadas pela Aranha
	IDENTIFICAR("Identifique-se"),																						   						
	SOU_ARANHA("Eu sou uma aranha"),																					    	 						
	AGENTE_SOLICITADO("Agente solicitado"),																				    
	AGENTE_INDISPONIVEL("Agente indisponível no momento"),

	//Mensagens enviadas pelo Zumbi
	MORDER("Ceeeeerebro, BLEEH!"),		
	DRACULA("I'm Dracula, bitch!"),							

	//Mensagens enviadas pelo Caçador
	PRENDER("Você está preso ser das trevas do carvão!"),																    		 					
	CURE_ESSE_ZUMBI("Curador, cure esse ser maligno aqui!"),															    								

	//Mensagens enviadas pelo Curador
	CURAR("Sai capeta desse corpo que não te pertence"),																    			 						

	//Mensagens enviadas pelo Questionador
	SEU_DESAFIO("Responde essa ai, respondedor!"),																		   	 						
	CERTO("Sua resposta está eeeeeeeeeeeeeeeeeeeexata! Ganhou! Ganhou! Ganhou! Ganhou!"),								    						
	ERRADO("Você errou! Perdeu um milhão de reais em barras de ouro, que vale mais do que dinheiro!"),					     						

	//Mensagens enviadas pelo Respondedor
	ME_DESAFIE("Me manda uma pergunta ai, questionador!"),																 						
	MINHA_RESPOSTA("Minha resposta é : "),																			 						

	//Mensagens padrões
	SOU_O_AGENTE("Prazer, eu sou o agente .... "),																									
	SOLICITACAO_DESCONHECIDA("Não atendo essa solicitação");																					

	MENSAGENS_AGENTES(String mensagem) {
		this.mensagem = mensagem;
	}

	private String mensagem;

	public String getMensagem() {
		return mensagem;
	}

}																		

```

> **Thread**
O conceito de Thread é o de um programa poder fazer duas coisas ao mesmo tempo. Um exemplo: suponha que o usuário mandou gerar um relatório em um sistema. Enquanto o sistema gera o relatório você deseja que uma barra de progresso seja mostrada informando para o usuário o progresso da geração do relatório. Veja que nesse caso o mesmo programa tem de fazer duas coisas ao mesmo tempo “gerar o relatório” e “atualizar a barra de status”. Isso é possível com Threads. Uma thread fica responsável por gerar o relatorio enquanto outra atualiza a barra de status.

#### Método acaoAgirImp
Esse método também é abstrato e deve ser implementado pelo agente que herdar da classe Agente;


----------

### Criando o Agente


No Eclipse abra a aba src/main/java  e vá até o pacote `br.ufg.si.ad.grupo2.modelo`. Clique com o auxiliar sobre o pacote, clique em `new` depois em `class`. Na tela que se abrir faça o seguinte:

![enter image description here](https://lh3.googleusercontent.com/-fwiM_v2U6uc/VRiLrw8rCBI/AAAAAAAABO0/agipyy-czeo/s0/3.png "eclipse")


1. Package tenha certeza de que está o caminho br.ufg.si.ad.grupo2.modelo. Esse é o pacote dos agentes;
2. Em Name coloque o nome do agente que você deseja implementar
3. Modifiers selecione Package. Isso é para que o agente só seja visualizado dentro do pacote onde ele foi implementado;
4. Em *Superclass* escreva `br.ufg.si.ad.grupo2.modelo.Agente`. Assim seu agente já vai herdar as coisas que foram implementadas na classe Agente.
5. Clique em *Finish*.

> Seu agente vai ser gerado. Note que ele já vem com dois métodos **agir()** e **acaoEscutarImp()**.