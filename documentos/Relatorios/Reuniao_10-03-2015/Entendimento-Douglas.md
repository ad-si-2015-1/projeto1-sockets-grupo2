Entendimento - Douglas Pose
########################

Sobre o que foi definido em reunião da nossa equipe é que irão
existir várias máquinas locais: Caçador, Zumbi, Perguntador e
Respondedor.

Essas máquinas irão se comunicar com a Aranha, que armazenará
os IPs e Portas de cada uma dessas máquinas.

Além disso essas máquinas estarão fazendo requisições em intervalos
de tempo de 5s, 10s e 15s. Esses tempos ainda estão a definir.
Essas requisições irão solicitar a Aranha o IP e Porta de uma das
máquinas que estão na rede. A Aranha devolvera essas informações.

Sendo assim se for o Perguntador, recebera as informações de um
Respondedor. Ele ira fazer uma pergunta, o Respondedor responde
e o Perguntador avalia se esta correta ou não.

O Zumbi receberá informações de qualquer máquina, sendo assim se
ela conseguir se conectar a algumas delas, a máquina será infectada
se tornando também um Zumbi.

O Caçador da mesma forma, ira receber informações de Zumbis e ficará
por conta de restaurar os Zumbis ao seu estado anterior.

Sendo assim se formará um ciclo infinito de perguntas respostas,
infecções e restaurações.

Caso tenha me enganado em alguma informações, fiquem a vontade para
correções.

Até logo.

/Douglas Pose - 110389
