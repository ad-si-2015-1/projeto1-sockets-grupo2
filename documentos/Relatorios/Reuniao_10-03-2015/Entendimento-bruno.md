Entedimento do projeto na vis�o de Bruno Nogueira
=================================================

Ser�o desenvolvido dois aplicativos: um cliente e outro servidor. No aplicativo cliente rodar� as inst�ncias dos agentes Questionadores, Respondedores, Ca�adores e Zumbi; Esses agentes se conectar�o a um servidor e os dados de sua conex�o(IP/PORTA) ser�o armazenados em uma lista de conex�es;

No servidor ser� implementado o agente Aranha. Esse agente vai percorrer a lista de conex�es em intervalos de tempos em tempos e pegar� um dado de conex�o. Esse dado ter� as informa��es para acessar um dos agentes clientes. Ao ser contatado por outro agente cliente solicitando informa��es de outros agentes para intera��o, o agente aranha dever� informar os dados de conex�o do �ltimo indiv�duo da lista que ele pegou;

Com as informa��es de conex�o, os agentes clientes criam canais de comunica��es entre eles para executar as a��es que foram programados (perguntar, infectar, curar, responder);

Se ocorre comunica��o entre dois agentes que n�o conversam, por exemplo, agente questiona um ca�ador, a comunica��o deve ser encerrada e o canal de comunica��o deve ser fechado;

#### Necessidade de levantamento
* Como ser� implementado o desafio entre agente questionador e agente respondedor
* Quais os intervalos entre os fluxos de a��es de cada agente;
* � poss�vel um agente cliente enviar e escutar conex�es atrav�s de uma mesma porta ou � necess�rio haver duas portas de comunica��o para cada cliente?
* � poss�vel que o agente aranha, ao inv�s de percorrer uma pilha de conex�es em uma rede percorra efetivamente a rede em busca de clientes?