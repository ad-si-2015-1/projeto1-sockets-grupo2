﻿Entendimento Guilherme Pinheiro
=========

*Em reunião realizada no dia 10/03 ficou claro no nosso projeto pontos como classes bases (agentes: aranha,
perguntador, respondedor, caçador e zumbi) onde ainda devemos estabelecar um protocolo de comunicação entre 
elas (um dos fatores chaves), o fato de todas serem armazenadas em um servidor central e a questão da "Aranha"
ficar varrendo uma pilha que irá conter IP/2 portas de cada agente, fornecendo assim, quando questionada, o último
agente localizado.

*Definir uma boa documentação inicial torna-se necessário para evitar confusões futuras e um descentralizamento
de atividades.