Entendimento do projeto - Daniel
====

### Proposta de solução

A solução proposta leva em consideração cinco personagens:
- Aranha
- Zumbi
- Caçador
- Questionador
- Respondedor

A solução proposta se compõe de um servidor e de vários clientes. O servidor será responsável pela execução do agente "aranha" e os clientes assumem aleatoriamente uma das demais funções.

A Aranha implementa uma pilha de elementos conhecidos e guarda X posições de outros agentes conhecidos. Seu IP é conhecido pelos outros agentes.

Os demais agentes implementam personalidades/comportamentos de acordo com sua natureza. Como um comportamento em comum todos eles, ao iniciar, contatam a Aranha e a informam do seu IP e de uma porta que servirá para comunicação com os outros agentes.

Os demais, conhecendo onde na rede está a aranha, obtem um IP/porta dela o elemento no topo da pilha e tentam comunicação com o indivíduo. A Aranha, ao fornecer o item o remove da pilha.

O indivíduo que obteve a posição na rede de outro agente tenta comunicar-se. Se a resposta divergir de uma que ele saiba interpretar (ex: um questionador fala com o caçador), a conversa é terminada e o ciclo reinicia.

#### Necessidade de levantamento
- Que agente é capaz de responder que outro agente.
- Funcionamento da infecção do zumbi. Contactar um zumbi te torna um zumbi?
- Formato da pergunta do questionador, da resposta do respondedor e da forma de retornar se ela foi correta.

### Fluxo de atividades
Também conhecido como [Git Flow](https://blogs.endjin.com/wp-content/uploads/2013/03/sbsgtgf-11-git-flow-release.png)

- A branch `master` representa um estado final. **Ninguém** jamais comita diretamente para ela. Todos os commits que ela recebe são rastreados por merge request.
- A partir do ultimo estado da master, criam-se branches com nome no formato `develop-xx`, onde `xx` é o numero ou nome da milestone definida para aquele ciclo.
- A partir da branch de milestone, os desenvolvedores e documentadores criam branches no formato `dev-nome`, sendo essas branches pessoais de trabalho. Os desenvolvedores comitam diretamente para suas branches, realizam push delas para o servidor e solicitam merge requests para a branch de milestone
- Ao final do ciclo definido pela milestone, um MR é aberto e aceito pelo mestre do repositório, integrando o trabalho da branch de milestone à `master`.
- Uma nova milestone é definida (muito possivelmente associando issues ao seu cumprimento) e uma nova branch é criada para ela. A branch da milestone anterior pode ser deletada se assim se preferir.
