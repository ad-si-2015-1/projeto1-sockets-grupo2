Projeto Aranha
==============

Justificativa
-------------

Sob a orientação do Prof. Marcelo Akira, na disciplina de Aplicações Distribuídas, Universidade de Goiás, primeiro período de 2015, foi solicitado aos alunos da turma a construção de um projeto com as seguintes características:


* Simulação de trabalho em uma empresa de desenvolvimento de agentes inteligentes, com classificação prévia e objetivos simples usando API de Sockets:
- aranha (crawler): varre a subrede em busca de outros agentes, procurando identificar o IP e a classificação. Quando questionado, fornece somente o último agente localizado. É imune a zumbis.
- respondedor: responde a desafios aritméticos fornecidos por um questionador;
- questionador: fornece desafios aritméticos solicitados e avalia se a resposta está correta;
- zumbi: envia um sinal que faz com que o agente que o contactou se torne um zumbi. Ou seja, não responde e nem questiona desafios aritméticos;
- caçador: envia um sinal que faz com que o zumbi seja preso. É imune ao zumbi;
- curador: envia um sinal que faz com que o zumbi se cure. É imune a zumbis.
O único que pode localizar e classificar agentes é a aranha e não pode ser morta.


Mediante essa solicitação, o grupo 2, composto pelos alunos Ana Letícia Herculano, Bruno Nogueira de Oliveira, Daniel Melo, Douglas Pose de Oliveira e Guilherme Pinheiro de Souza e Silva se propos a desenvolver uma solução sob o projeto que esse documento inicia;

Objetivo
--------
Desenvolver um aplicativo distribuído que tenha interações próprias e seguem as especificações dispostas pelo Prof. Marcelo Akira, no item anterior;


Requisitos (versão inicial)
----------------------------
A solução proposta leva em consideração cinco personagens:
- Aranha - Responsável informar aos agentes clientes os endereços e portas de outros agentes;
- Zumbi - Irá "infectar" outros agentes, impossibilitando-os de respostas;
- Caçador - Quem irá atrás do Zumbi, evitando a infecção em outros agentes
- Curador - Quem irá retornar um Zumbi infectado para seu estado original;
- Questionador - Quem irá realizar perguntas
- Respondedor - Quem irá responder as perguntas

A solução proposta se compõe de um servidor e de vários clientes. O servidor será responsável pela execução do agente "aranha" e os clientes assumem aleatoriamente uma das demais funções.

A Aranha implementa uma pilha de elementos conhecidos e guarda X posições de outros agentes conhecidos. Seu IP é conhecido pelos outros agentes.

Os demais agentes implementam personalidades/comportamentos de acordo com sua natureza. Como um comportamento em comum todos eles, ao iniciar, contatam a Aranha e a informam do seu IP e de uma porta que servirá para comunicação com os outros agentes.

Além disso essas máquinas estarão fazendo requisições em intervalos de tempo. Esses tempos ainda estão a definir. Essas requisições irão solicitar a Aranha o IP e Porta de uma das máquinas que estão na rede. A Aranha devolvera essas informações.

O indivíduo que obteve a posição na rede de outro agente tenta comunicar-se. Se a resposta divergir de uma que ele saiba interpretar (ex: um questionador fala com o caçador), a conversa é terminada e o ciclo reinicia.

As comunicações funcionarão por protocolos TCP/IP em rede interna e controlada;

Cronograma de marcos (Milestones)
---------------------------------
As entregas estão agendadas para as seguintes datas:
* 17/03/2015 - Milestone 01 - Entrega de documentação básica;
* 20/03/2015 - Milestone 02 - Alinhamento final entre solução proposta e solução definitiva
* 28/03/2015 - Milestone 03 - Desenvolvimento da solução;
* 29/03/2015 - Milestone 04 - Testes, Correção de erros, simulação da aplicação em funcionamento;
* 30/03/2015 - Milestone 05 - Entrega definitiva do projeto;

Papeis dos colaboradores
------------------------
* Ana Letícia Herculano - Desenvolvedora;
* Bruno Nogueira de Oliveira - Lider do grupo e desenvolvedor;
* Daniel Melo - Desenvolvedor;
* Douglas Pose de Oliveira - Desenvolvedor
- Guilherme Pinheiro de Souza e Silva - Desenvolvedor;



