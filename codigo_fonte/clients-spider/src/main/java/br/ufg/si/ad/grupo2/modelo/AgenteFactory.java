package br.ufg.si.ad.grupo2.modelo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.modelo.exception.DraculaException;


public class AgenteFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(AgenteFactory.class);

	private static AgenteFactory factory;

	private AgenteFactory() {
		// Singleton
	}

	public static AgenteFactory getAgenteFactory() {
		if(factory == null) {
			LOGGER.debug("Não existe uma fábrica instanciada. Vou criar uma!");
			factory = new AgenteFactory();
		}
		LOGGER.debug("Devolvendo a instância da Fábrica para uso");
		return factory;
	}

	public Agente criarAgente(int idAgente) {
		LOGGER.debug("Criando agente à partir do ID do Agente");
		for(NOME_AGENTES tpAgente : NOME_AGENTES.values()) {
			if(tpAgente.getNumId() == idAgente){
				return criarAgente(tpAgente);
			}
		}
		return null;
	}

	/** Instancia um agente conforme o nome dele*/
	public Agente criarAgente(String nomeAgente) {
		LOGGER.debug("Criando agente à partir do NOME enumerado do Agente");
		for(NOME_AGENTES tpAgente : NOME_AGENTES.values()) {
			if(tpAgente.getNome().toLowerCase().equals( nomeAgente.toLowerCase() )){
				return criarAgente(tpAgente);
			}
		}
		return null;
	}

	/** Instancia um agente aleatóriamente */
	public Agente criarAgente() {
		LOGGER.debug("Criando agente de maneira aleatória");
		return criarAgente( choose() );
	}

	/** Instancia um agente conforme ID do agente*/
	public Agente criarAgente(NOME_AGENTES tpAgente) {

		Agente agente = null;

		switch(tpAgente)  {
		case ARANHA:
			LOGGER.debug("Criando agente Aranha");
			agente = new Aranha();
			break;

		case QUESTIONADOR:
			LOGGER.debug("Criando agente Questionador");
			agente = new Questionador();
			break;

		case RESPONDEDOR:
			LOGGER.debug("Criando agente Respondedor");
			agente = new Respondedor();
			break;

		case ZUMBI:
			LOGGER.debug("Criando agente Zumbi");
			agente = new Zumbi();
			break;

		case CACADOR:
			LOGGER.debug("Criando agente Caçador");
			agente = new Cacador();
			break;

		case CURADOR:
			LOGGER.debug("Criando agente Curador");
			agente = new Curador();
			break;

		default:
			LOGGER.debug("Se não consigo instanciar nada não vou iniciar");
			throw new RuntimeException();
		}

		return agente;

	}

	private int choose() {
		return (int) (Math.random() * 100) % NOME_AGENTES.values().length;
	}

	protected void contaminar(Agente agente, NOME_AGENTES tipoAgente) {
		Zumbi zumbi = new Zumbi();
		zumbi.setAngenteAnterior(tipoAgente);
		agente = zumbi;
	}

	protected void curar(Agente agente, Zumbi mesmoAgente) throws DraculaException {
		if (mesmoAgente.getAngenteAnterior().equals(NOME_AGENTES.QUESTIONADOR)){
			agente = getAgenteFactory().criarAgente(NOME_AGENTES.QUESTIONADOR);
		} else if(mesmoAgente.getAngenteAnterior().equals(NOME_AGENTES.RESPONDEDOR)) {
			agente = getAgenteFactory().criarAgente(NOME_AGENTES.RESPONDEDOR);
		} else {
			throw new DraculaException(MENSAGENS_AGENTES.DRACULA);
		}

	}

}
