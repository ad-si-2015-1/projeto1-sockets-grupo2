package br.ufg.si.ad.grupo2.utils;

import java.io.Serializable;

import br.ufg.si.ad.grupo2.modelo.MENSAGENS_AGENTES;
import br.ufg.si.ad.grupo2.modelo.NOME_AGENTES;

/** Classe que representa um objeto de troca de mensagens que será utilizado pela aplicação */
public class Envelope implements Serializable{

	private static final long serialVersionUID = -441561897767381207L;

	private String ip;
	private Integer porta;
	private NOME_AGENTES instanciaAgente;
	private MENSAGENS_AGENTES mensagem;
	private String mensagemPersonalizada;

	public Envelope(String ip, Integer porta, NOME_AGENTES instanciaAgente,
			MENSAGENS_AGENTES mensagem, String mensagemPersonalizada) {
		this.ip = ip;
		this.porta = porta;
		this.instanciaAgente = instanciaAgente;
		this.mensagem = mensagem;
		this.mensagemPersonalizada = mensagemPersonalizada;
	}

	public String getIp() {
		return ip;
	}
	public Integer getPorta() {
		return porta;
	}
	public NOME_AGENTES getInstanciaAgente() {
		return instanciaAgente;
	}
	public MENSAGENS_AGENTES getMensagem() {
		return mensagem;
	}
	public String getMensagemPersonalizada() {
		return mensagemPersonalizada;
	}

	public String toString() {
		try {
			return String.format("%s %s %s %s %s", getIp(), getPorta(), getInstanciaAgente(), getMensagem(), getMensagemPersonalizada());
		} catch (Exception e) {
			return "sem toString disponivel para a mensagem";
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((instanciaAgente == null) ? 0 : instanciaAgente.hashCode());
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result
				+ ((mensagem == null) ? 0 : mensagem.hashCode());
		result = prime * result + ((porta == null) ? 0 : porta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Envelope other = (Envelope) obj;
		if (instanciaAgente != other.instanciaAgente)
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (mensagem != other.mensagem)
			return false;
		if (porta == null) {
			if (other.porta != null)
				return false;
		} else if (!porta.equals(other.porta))
			return false;
		return true;
	}

}
