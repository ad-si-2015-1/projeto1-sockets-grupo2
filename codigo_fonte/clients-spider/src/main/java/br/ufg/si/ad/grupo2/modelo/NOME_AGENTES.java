package br.ufg.si.ad.grupo2.modelo;

/** Enum que contém as implementações prontas das classes Agentes*/
public enum NOME_AGENTES {
	ARANHA(0, "aranha"),
	ZUMBI(1, "zumbi"),
	RESPONDEDOR(2, "respondedor"),
	QUESTIONADOR(3, "questionador"),
	CACADOR(4, "cacador"),
	CURADOR(5, "curador");


	private int numId;
	private String nome;

	NOME_AGENTES(int numId, String nome) {
		this.numId = numId;
		this.nome = nome;
	}

	public int getNumId() {
		return numId;
	}

	public String getNome() {
		return nome;
	}
}