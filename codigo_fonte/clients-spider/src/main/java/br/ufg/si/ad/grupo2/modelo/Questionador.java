package br.ufg.si.ad.grupo2.modelo;

import java.io.IOException;
import java.net.Socket;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.utils.Envelope;
import br.ufg.si.ad.grupo2.utils.SocketUtils;

public class Questionador extends Agente {

	private static final Logger LOGGER = LoggerFactory.getLogger(Respondedor.class);

	/* Variável definida para guardar as respostas dos desafios */
	private int resposta;

	Questionador() {
		super();
		LOGGER.info("Esperando os pimpolhos com as dúvidas... ");
	}

	@Override
	void acaoEscutarImp(Socket socket) {
		try {
			Envelope incommingMessage = SocketUtils.aguardarMensagem(socket);
			LOGGER.debug("Recebida mensagem {}", incommingMessage);

			MENSAGENS_AGENTES mensagem = incommingMessage.getMensagem();
			switch (mensagem) {
			case IDENTIFICAR:

				LOGGER.info("Chesus!!! Uma aranha!! Que horror!");
				identificarSe(socket, incommingMessage);
				LOGGER.debug("Ufa.. ela foi embora...");

				break;
			case ME_DESAFIE:
				LOGGER.info("Um badeco me achou... enviando desafio");
				enviarDesafio(socket, incommingMessage);

				incommingMessage = SocketUtils.aguardarMensagem(socket);

				if(avaliarDesafio(incommingMessage)) {
					enviarResposta(socket, true);
				} else {
					enviarResposta(socket, false);
				}

				break;

			case MORDER:
				LOGGER.info("Meu Deus... um Zumbi!! AAAAAAAAAAA");

				Envelope resposta = buildEnvelope(NOME_AGENTES.QUESTIONADOR, MENSAGENS_AGENTES.FUI_MORDIDO, null);

				SocketUtils.enviarMensagem(socket, resposta);

				LOGGER.info("Agente infectado");
				AgenteFactory.getAgenteFactory().contaminar(this, NOME_AGENTES.RESPONDEDOR);

			default:
				LOGGER.warn("Nao sei responder a esse comando. Ignorando...");
				SocketUtils.enviarMensagem(socket, mensagemDeIgnorancia());
				break;
			}

		} catch (Exception e) {
			LOGGER.error("Erro ao escutar", e);
		}

	}

	@Override
	void acaoAgirImp() {
		LOGGER.debug("La la la la.. sou um questionador feliz...");

		try {
			Thread.sleep(10000);
		} catch (Exception e) {
			LOGGER.debug("Opa, nao consigo esperar... coisas estranhas podem acontecer. ", e.getMessage());
		}

	}

	private void enviarDesafio(Socket socket, Envelope incommingMessage) {
		LOGGER.debug("Iniciando geracao de desafios");
		String desafio = gerarDesafio();  // TODO gerar desafios aleatoriamente
		Envelope desafioProposto = buildEnvelope(NOME_AGENTES.RESPONDEDOR, MENSAGENS_AGENTES.SEU_DESAFIO, desafio);
		try {
			SocketUtils.enviarMensagem(socket, desafioProposto);
			LOGGER.info("Desafio proposto: Quanto é " + desafio + " ?");
		} catch (Exception e) {
			LOGGER.error("Erro ao enviar um desafio", e);
		}

	}

	private String gerarDesafio() {

		Random randomico = new Random();

		int numero1 = randomico.nextInt();
		int numero2 = randomico.nextInt();

		String operando = "+";

		switch(randomico.nextInt() % 4) {
		case 0:
			operando = "+";
			resposta = numero1 + numero2;
			break;

		case 1:
			operando = "-";
			resposta = numero1 - numero2;
			break;

		case 2:
			operando = "*";
			resposta = numero1 * numero2;
			break;

		case 3:
			operando = "/";
			resposta = numero1 / numero2;
			break;

		default:
			operando = "+";
			resposta = numero1 + numero2;
		}

		return (String.valueOf(numero1) + operando + String.valueOf(numero2)).trim();
	}

	private Boolean avaliarDesafio(Envelope incommingMessage) {
		LOGGER.info("Avaliando resposta!");
		if(incommingMessage.getMensagemPersonalizada().equals( String.valueOf(resposta) ) ) {
			return true;
		}
		return false;
	}

	private void enviarResposta(Socket socket, boolean correto) {
		try {
			Envelope resposta;
			if(correto) {
				LOGGER.info("Respondedor acertou");
				resposta = buildEnvelope(NOME_AGENTES.QUESTIONADOR, MENSAGENS_AGENTES.CERTO, null);
			} else {
				LOGGER.info("Respondedor errou" );
				resposta = buildEnvelope(NOME_AGENTES.QUESTIONADOR, MENSAGENS_AGENTES.ERRADO, null);
			}
			SocketUtils.enviarMensagem(socket, resposta);

		} catch (IOException e) {
			LOGGER.error("Não consegui enviar a resposta!");
		}
	}

	private void identificarSe(Socket socket, Envelope incommingMessage) {
		LOGGER.debug("Guardando endereco da aranha para uso futuro");
		guardarEnderecoAranha(socket, incommingMessage);
		LOGGER.info("Identificando-se para a aranha na rede");
		Envelope identificacao = buildEnvelope(NOME_AGENTES.QUESTIONADOR, MENSAGENS_AGENTES.SOU_O_AGENTE, null);

		try {
			LOGGER.debug("Respondendo a aranha com a mensagem {}", identificacao);
			SocketUtils.enviarMensagem(socket, identificacao);
		} catch (Exception e) {
			LOGGER.error("Erro ao enviar identificacao para a aranha", e);
		}
	}

	private Envelope mensagemDeIgnorancia() {
		return buildEnvelope(NOME_AGENTES.QUESTIONADOR, MENSAGENS_AGENTES.SOLICITACAO_DESCONHECIDA, null);
	}

}
