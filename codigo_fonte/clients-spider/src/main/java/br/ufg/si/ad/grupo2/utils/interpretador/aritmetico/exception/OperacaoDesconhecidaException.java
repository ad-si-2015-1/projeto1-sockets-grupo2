package br.ufg.si.ad.grupo2.utils.interpretador.aritmetico.exception;

public class OperacaoDesconhecidaException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OperacaoDesconhecidaException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public OperacaoDesconhecidaException() {
		super();
	}

	public OperacaoDesconhecidaException(String string) {
		super(string);
	}

	public String getLocalizedMessage(){
		return "A operacao escolhida não é suportada";
	}

}
