Interpretador aritmético
====
Autor: Daniel Melo ([@danielmelogpi](https://gitlab.com/u/danielmelogpi))


Este pacote inclui classes capazes de tomar uma expressão em forma de string como `1+6` ou `2^3` e devolver o resultado da operação ou as exceções adequadas.

A pasta de testes possui testes para as seis operações implementadas.

Suporte inclui as seguintes operações:

- Adição (+): `2+3`
- Subtração (-): `2-3`
- Multiplicação (\*): `2 * 3`
- Divisão (/): `2/3`
- Exponenciação (^): `2^3` (2 elevado à 3)
- Raiz enésima (#) : `3#2` (raiz quadrada de 3), `8#3` (raiz cubica de 8)


Os principais métodos a serem usados são `InterpretadorAritmetico.calcularDouble` e `InterpretadorAritmetico.calcularString`, passando como único argumento o desafio aritmético. Veja a classe `InterpretadorAritmeticoTest.java` para detalhes de uso.

Os testes podem ser rodados no eclipse usando `run-tests.launch` (usa JUnit 4).

Exemplo:

```java

Double resultado = InterpretadorAritmetico.calcularDouble("5+5");
/** resultado vale "10.0" **/

```