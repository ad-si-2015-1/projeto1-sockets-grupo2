package br.ufg.si.ad.grupo2.modelo;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.utils.Envelope;
import br.ufg.si.ad.grupo2.utils.SocketUtils;

class Curador extends Agente {

	private static final Logger LOGGER = LoggerFactory.getLogger(Curador.class);

	private int timeToSleep = 5000;

	Curador() {
		super();
	}

	@Override
	void acaoEscutarImp(Socket socket) {

		try {
			Envelope recebida = SocketUtils.aguardarMensagem(socket);
			LOGGER.debug("Recebida mensagem {}", recebida);

			Envelope resposta;

			switch(recebida.getMensagem()) {
			case IDENTIFICAR:
				LOGGER.debug("Eu sou um ser sagrado que fala com uma Aranha!! o.O");
				setEnderecoAranha(recebida.getIp());
				setPortaAranha(recebida.getPorta());

				resposta = new Envelope(this.getEndereco().getHostAddress(), this.getPorta(),
										NOME_AGENTES.CURADOR, MENSAGENS_AGENTES.SOU_O_AGENTE, null);
				SocketUtils.enviarMensagem(socket, resposta);

				LOGGER.debug("Agora que já conhecem meu caminho, estou indo salvar esses pecadores!");
				setAtivo(true);

				break;

			case CURE_ESSE_ZUMBI:
				LOGGER.debug("Um Caçador do Senhor solicita minha ajuda para exorcizar um demônio!");
				curarZumbi(recebida);

				break;

			default:


			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	void acaoAgirImp() {

		try {

			/* Solicitando alvo para Aranha */
			Socket socket = new Socket(this.getEnderecoAranha(), this.getPortaAranha());

			Envelope envio = new Envelope(this.getEndereco().getHostAddress(), this.getPorta(),
					NOME_AGENTES.ZUMBI, MENSAGENS_AGENTES.INFORME_UM_AGENTE, null);

			SocketUtils.enviarMensagem(socket, envio);

			Envelope retorno = SocketUtils.aguardarMensagem(socket);

			socket.close();

			/* Tentando curar o Zumbi*/
			curarZumbi(retorno);

			Thread.sleep(timeToSleep);


		} catch (UnknownHostException e) {
			LOGGER.error("Não conheço o host de destino: ", e.getCause());

		} catch (IOException e) {
			LOGGER.error("Problema com a entrada e saida de informações: ", e.getCause());

		} catch (ClassNotFoundException e) {
			LOGGER.error("Por algum motivo a classe solicitada não foi encontrada! ", e.getCause());

		} catch (InterruptedException e) {
			LOGGER.error("Infelizmente não posso esperar! O trabalho me chama! ", e.getCause() );
		}

	}

	private void curarZumbi(Envelope recebida) {


		try {

			LOGGER.debug("Iniciando a oração... ");
			Envelope curar;

			Socket socketZumbi = new Socket(recebida.getIp(), recebida.getPorta());
			curar = buildEnvelope(NOME_AGENTES.CURADOR, MENSAGENS_AGENTES.CURAR, null);

			LOGGER.debug(MENSAGENS_AGENTES.CURAR.getMensagem()); // veja o que sairá em produção!
			SocketUtils.enviarMensagem(socketZumbi, curar);

			Envelope retorno = SocketUtils.aguardarMensagem(socketZumbi);
			LOGGER.debug(retorno.getMensagem().getMensagem());

		} catch (UnknownHostException e) {
			LOGGER.error("Não conheço o host de destino: ", e.getCause());

		} catch (IOException e) {
			LOGGER.error("Problema com a entrada e saida de informações: ", e.getCause());

		} catch (ClassNotFoundException e) {
			LOGGER.error("Por algum motivo a classe solicitada não foi encontrada! ", e.getCause());

		}



	}

}

