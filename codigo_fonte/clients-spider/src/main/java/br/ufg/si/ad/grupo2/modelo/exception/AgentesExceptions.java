package br.ufg.si.ad.grupo2.modelo.exception;

import br.ufg.si.ad.grupo2.modelo.MENSAGENS_AGENTES;

public class AgentesExceptions extends Exception {

	private static final long serialVersionUID = -9048545642345508863L;

	public AgentesExceptions() {
		super();
	}

	public AgentesExceptions(MENSAGENS_AGENTES message) {
		super(message.getMensagem());
	}
}
