package br.ufg.si.ad.grupo2.modelo.exception;

import br.ufg.si.ad.grupo2.modelo.MENSAGENS_AGENTES;

public class AgenteIndisponivelException extends Exception {

	private static final long serialVersionUID = 4611888285220274903L;

	public AgenteIndisponivelException() {
		super();
	}

	public AgenteIndisponivelException(MENSAGENS_AGENTES message) {
		super(message.getMensagem());
	}

}
