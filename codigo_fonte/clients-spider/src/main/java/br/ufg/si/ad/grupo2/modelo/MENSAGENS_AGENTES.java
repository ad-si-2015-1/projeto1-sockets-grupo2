package br.ufg.si.ad.grupo2.modelo;

public enum MENSAGENS_AGENTES {
																														/* 	   ======= Destinatário ========		*/
	//Mensagens de contaminação
	SOU_IMUNE("Não é possível me contaminar"),																			/*	  Zumbi									*/
	FUI_MORDIDO("Fui mordido e agora quero um cerebro"), 																/*    Zumbi									*/
	INFORME_UM_AGENTE("me informe um agente"),																			/*    Aranha		 						*/

	//Mensagens enviadas pela Aranha
	IDENTIFICAR("Identifique-se"),																						/*    Qualquer agente 						*/
	SOU_ARANHA("Eu sou uma aranha"),																					/*    Aranha		 						*/
	AGENTE_SOLICITADO("Agente solicitado"),																				/*    Zumbi, Cacador, Curador, Questionador */
	AGENTE_INDISPONIVEL("Agente indisponível no momento"),																/*    Zumbi, Cacador, Curador, Questionador */

	//Mensagens enviadas pelo Zumbi
	MORDER("Ceeeeerebro, BLEEH!"),																						/*    Questinador, Respondedor 				*/
	DRACULA("I'm Dracula, bitch!"),																						/*    Curador, Caçador 						*/

	//Mensagens enviadas pelo Caçador
	PRENDER("Você está preso ser das trevas do carvão!"),																/*    Zumbi			 						*/
	CURE_ESSE_ZUMBI("Curador, cure esse ser maligno aqui!"),															/*    Curador								*/

	//Mensagens enviadas pelo Curador
	CURAR("Sai capeta desse corpo que não te pertence"),																/*    Zumbi			 						*/

	//Mensagens enviadas pelo Questionador
	SEU_DESAFIO("Responde essa ai, respondedor!"),																		/*    Respondedor	 						*/
	CERTO("Sua resposta está eeeeeeeeeeeeeeeeeeeexata! Ganhou! Ganhou! Ganhou! Ganhou!"),								/*    Respondedor	 						*/
	ERRADO("Você errou! Perdeu um milhão de reais em barras de ouro, que vale mais do que dinheiro!"),					/*    Respondedor	 						*/

	//Mensagens enviadas pelo Respondedor
	ME_DESAFIE("Me manda uma pergunta ai, questionador!"),																/*    Questionador	 						*/
	MINHA_RESPOSTA("Minha resposta é : "),																			/*    Questionador	 						*/

	//Mensagens padrões
	SOU_O_AGENTE("Prazer, eu sou o agente .... "),																		/*    Aranha  								*/
	SOLICITACAO_DESCONHECIDA("Não atendo essa solicitação");															/* 	  TODOS 								*/

	MENSAGENS_AGENTES(String mensagem) {
		this.mensagem = mensagem;
	}

	private String mensagem;

	public String getMensagem() {
		return mensagem;
	}

}
