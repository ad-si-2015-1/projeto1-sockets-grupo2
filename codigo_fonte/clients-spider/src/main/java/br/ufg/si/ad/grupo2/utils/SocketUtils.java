package br.ufg.si.ad.grupo2.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SocketUtils {
	
	Logger logger = LoggerFactory.getLogger(SocketUtils.class);

	/** Método estático responsável por enviar mensagens através de um socket que é passado por argumento
	 *
	 * @param Socket : socket por onde a conexão será feita
	 * @param Envelope : Conteúdo da mensagem que será enviada
	 *
	 * @author Bruno Nogueira*/
	public static void enviarMensagem(Socket socket, Envelope mensagem) throws IOException {

		OutputStream out = socket.getOutputStream();
		ObjectOutputStream oout = new ObjectOutputStream(out);
		oout.writeObject(mensagem);

	}

	/** Método estático responsável por receber uma mensagem através de um socket existente que aguarda resposta
	 *
	 * @param Socket : por onde a resposta é aguardada
	 * @return Envelope : objeto envelope onde a mensagem está construída
	 *
	 * @author Bruno Nogueira*/
	public static Envelope aguardarMensagem(Socket socket) throws IOException, ClassNotFoundException {

		InputStream in = socket.getInputStream();
		ObjectInputStream oin = new ObjectInputStream(in);
		return (Envelope) oin.readObject();

	}
	
	/** Transmite tratando as exceções.
	 * Em caso de sucesso, retorna um Envelope. Em caso de alguma falha, retorna nulo.
	 * @see SocketUtils.enviarMensagem();
	 * 
	 * @param socket Socket a usar na transmissao mensagem
	 * @param mensagem Mensagem a ser transmitida
	 * @return Envelope
	 */
	public boolean envioSilencioso(Socket socket, Envelope mensagem) {
		try {
			enviarMensagem(socket, mensagem);
			return true;
		} catch (Exception e) {
			logger.error("Erro ao enviar mensagem", e);
			return false;
		}
	}
	
	/** Recebe uma mensagme tratando as exceções.
	 * Em caso de sucesso, retorna um Envelope. Em caso de alguma falha, retorna nulo.
	 * 	 * @see SocketUtils.enviarMensagem();
	 * 
	 * @param socket Socket a usar na transmissao mensagem
	 * @param mensagem Mensagem a ser transmitida
	 * @return Envelope
	 */
	public Envelope recebimentoSilencioso(Socket socket) {
		try {
			Envelope mensagem = aguardarMensagem(socket);
			return mensagem;
		} catch (Exception e) {
			logger.error("Erro ao receber uma mensagem", e);
			return null;
		}
	}

}
