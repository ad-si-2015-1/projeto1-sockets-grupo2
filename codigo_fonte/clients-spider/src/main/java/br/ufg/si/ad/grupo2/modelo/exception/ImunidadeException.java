package br.ufg.si.ad.grupo2.modelo.exception;

import br.ufg.si.ad.grupo2.modelo.MENSAGENS_AGENTES;

public class ImunidadeException extends AgentesExceptions {

	private static final long serialVersionUID = 4874549398796660638L;

	public ImunidadeException() {
		super();
	}

	public ImunidadeException(MENSAGENS_AGENTES message) {
		super(message);
	}

}
