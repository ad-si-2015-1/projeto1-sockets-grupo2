package br.ufg.si.ad.grupo2.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.modelo.Agente;
import br.ufg.si.ad.grupo2.modelo.AgenteFactory;
import br.ufg.si.ad.grupo2.modelo.ThreadAgir;
import br.ufg.si.ad.grupo2.modelo.ThreadOuvir;

public class SpiderMain {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpiderMain.class);

	public static void main(String[] args) {

		LOGGER.debug("################## Iniciando aplicação ##################");
		Agente agenteMain;
		if(args.length == 0) {
			agenteMain = AgenteFactory.getAgenteFactory().criarAgente();
		}
		else {
			agenteMain = AgenteFactory.getAgenteFactory().criarAgente(args[0]);
		}

		/* Iniciando Threads de escuta */
		LOGGER.debug("Instanciando Threads de escuta e execução");
		new Thread( new ThreadOuvir(agenteMain) ).start();
		new Thread( new ThreadAgir(agenteMain) ).start();
		
		LOGGER.debug("O agente {} foi instanciado no IP {} na porta {}" , 
				agenteMain.getClass().getName(), 
				agenteMain.getEndereco().getHostAddress(),
				agenteMain.getPorta());			
		

	}

}
