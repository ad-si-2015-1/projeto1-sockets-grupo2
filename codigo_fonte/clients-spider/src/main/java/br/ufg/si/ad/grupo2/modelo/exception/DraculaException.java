package br.ufg.si.ad.grupo2.modelo.exception;

import br.ufg.si.ad.grupo2.modelo.MENSAGENS_AGENTES;

public class DraculaException extends AgentesExceptions {

	private static final long serialVersionUID = 3248761334353773061L;

	public DraculaException() {
		super();
	}

	public DraculaException(MENSAGENS_AGENTES message) {
		super(message);
	}

}
