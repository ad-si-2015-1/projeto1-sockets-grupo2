package br.ufg.si.ad.grupo2.modelo;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.modelo.exception.DraculaException;
import br.ufg.si.ad.grupo2.utils.Envelope;
import br.ufg.si.ad.grupo2.utils.SocketUtils;

class Zumbi extends Agente {

	private static final Logger LOGGER = LoggerFactory.getLogger(Zumbi.class);
	protected NOME_AGENTES angenteAnterior = null;

	/* Define o tempo que o Zumbi vai dormir entre um ciclo e outro */
	private int timeToSleep = 40000;

	Zumbi() {
		super();
		LOGGER.info("EEEEERRRR! Ceeeeerebro... ");
	}

	@Override
	void acaoEscutarImp(Socket socket) {

		try {
			Envelope recebida = SocketUtils.aguardarMensagem(socket);
			LOGGER.debug("Recebida mensagem {}", recebida);

			Envelope resposta;

			switch(recebida.getMensagem()) {
			case IDENTIFICAR:
				LOGGER.info("Araaaanhaa... não tem ceeeeerebro!! bleeeee... ");

				guardarEnderecoAranha(socket, recebida);

				resposta = buildEnvelope(NOME_AGENTES.ZUMBI, MENSAGENS_AGENTES.SOU_O_AGENTE, null);
				SocketUtils.enviarMensagem(socket, resposta);

				LOGGER.info("Ceeerebroooo... quero ceeeereeebrrooooo...");
				setAtivo(true);

				break;

			case PRENDER:
				LOGGER.info("Bblleeeeee.... me peggaaarraammm .... eeeeehhh");
				timeToSleep = 60000;

				break;

			case CURAR:
				try {
					AgenteFactory.getAgenteFactory().curar(this, this);
				} catch (DraculaException e) {
					LOGGER.info(MENSAGENS_AGENTES.DRACULA.getMensagem());
				}
				break;

			default:
				LOGGER.info("Bleeeee... desconheço essa mensagemmm... ignoraaaaando....");
				LOGGER.debug("Mensagem: " + recebida.getMensagem().getMensagem());
				resposta = buildEnvelope(NOME_AGENTES.ZUMBI, MENSAGENS_AGENTES.SOLICITACAO_DESCONHECIDA, null);
				SocketUtils.enviarMensagem(socket, resposta);

			}

		} catch (ClassNotFoundException e) {
			LOGGER.error("Por algum motivo a classe solicitada não foi encontrada! ", e.getCause());

		} catch (IOException e) {
			LOGGER.error("Problema com a entrada e saida de informações: ", e.getCause());

		}

	}

	@Override
	void acaoAgirImp() {

		try {

			if(timeToSleep != 4000) {
				LOGGER.info("Estava presoooo... agora estou liiiivree... ceeeeeerebro......");
				timeToSleep = 4000;
			}

			LOGGER.info("Solicitando um prato do menu.... ");
			Socket socket = new Socket(this.getEnderecoAranha(), this.getPortaAranha());

			Envelope envio = new Envelope(this.getEndereco().getHostAddress(), this.getPorta(),
					null, MENSAGENS_AGENTES.INFORME_UM_AGENTE, null);

			SocketUtils.enviarMensagem(socket, envio);

			Envelope resposta = SocketUtils.aguardarMensagem(socket);

			socket.close();


			/* Agora manda a mensagem de contaminação */
			LOGGER.info("Hora do jantaaarrrrr... ceeeeeerebro! ");
			socket = new Socket(resposta.getIp(), resposta.getPorta());

			envio = new Envelope(this.getEndereco().getHostAddress(), this.getPorta(),
									NOME_AGENTES.ZUMBI, MENSAGENS_AGENTES.MORDER, null);

			SocketUtils.enviarMensagem(socket, envio);

			resposta = SocketUtils.aguardarMensagem(socket);

			if( resposta.getMensagem().equals( MENSAGENS_AGENTES.SOU_IMUNE )) {
				LOGGER.info("Não consegui commerrr esse cceeeeerebrrroo!");
			} else {
				LOGGER.info("Comi um ceeeeerebrrrrooo... ");
			}

			LOGGER.debug("#partiu tirar uma sonequinha!!");
			Thread.sleep(timeToSleep);

		} catch (UnknownHostException e) {
			LOGGER.error("Não conheço o host de destino: ", e);

		} catch (IOException e) {
			LOGGER.error("Problema com a entrada e saida de informações: ", e);

		} catch (ClassNotFoundException e) {
			LOGGER.error("Por algum motivo a classe solicitada não foi encontrada! ", e);

		} catch (InterruptedException e) {
			LOGGER.error("Eu quero mais ceeeeeerrebrrroooo ", e );

		} finally {
			try {
				Thread.sleep(timeToSleep);
			} catch (InterruptedException e) {
				LOGGER.error("Não paro de querer ceeeerreeeeebrrrrooooo");
			}
		}

	}

	NOME_AGENTES getAngenteAnterior() {
		return angenteAnterior;
	}

	void setAngenteAnterior(NOME_AGENTES angenteAnterior) {
		this.angenteAnterior = angenteAnterior;
	}



}
