package br.ufg.si.ad.grupo2.modelo;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.utils.Envelope;
import br.ufg.si.ad.grupo2.utils.SocketUtils;

public class Aranha extends Agente {

	private static final Logger LOGGER = LoggerFactory.getLogger(Respondedor.class);
	private int timeToSleep = 10000;

	private List<Envelope> agentesIdentificados = new ArrayList<Envelope>();

	/* Um 'ponteiro' para uma posição da lista de agentes identificados */
	private int posicaoLista = 0;

	Aranha() {
		super();
		setAtivo(true);
		LOGGER.info("Iniciando busca na teia... ops, rede");
	}

	@Override
	void acaoEscutarImp(Socket socket) {

		try {
			Envelope recebida = SocketUtils.aguardarMensagem(socket);
			LOGGER.debug("Recebendo um contato {}", recebida);

			Envelope resposta;

			switch(recebida.getMensagem()) {
			case IDENTIFICAR:
				LOGGER.info("Recebendo um contato de outra Aranha");
				resposta = buildEnvelope(NOME_AGENTES.ARANHA, MENSAGENS_AGENTES.SOU_ARANHA, null);

				LOGGER.debug("Duas aranhas conversando... um que delícia!!!");
				SocketUtils.enviarMensagem(socket, resposta);
				break;

			/* Envia o endereço IP, Porta e Tipo do Agente que ela recebeu requisição */
			case INFORME_UM_AGENTE:
				LOGGER.info("Recebendo uma solicitação de um agente. Esse aqui deseja um " + (recebida.getInstanciaAgente().getNome() == null ? "Qualquer" : recebida.getInstanciaAgente().getNome()));
				resposta = devolverAgente(recebida.getInstanciaAgente());

				SocketUtils.enviarMensagem(socket, resposta);

				break;

			default:
				resposta = new Envelope(this.getEndereco().getHostAddress(), this.getPorta(), NOME_AGENTES.ARANHA, MENSAGENS_AGENTES.SOLICITACAO_DESCONHECIDA, null);

				SocketUtils.enviarMensagem(socket, resposta);
			}
		} catch (ClassNotFoundException e) {
			LOGGER.error("Não encontrei a classe solicitada. ", e);
		} catch (IOException e) {
			LOGGER.error("Problema de I/O ", e);
		}

	}

	@Override
	void acaoAgirImp() {

		Enumeration<NetworkInterface> nis = null;

		while (getAtivo()) {
			try {
				nis = NetworkInterface.getNetworkInterfaces();
			} catch (SocketException e) {
				String message = "Não encontrado nenhuma interface disponível";
				throw new RuntimeException(message, e);
			}

			while (nis.hasMoreElements()) {
				NetworkInterface ni = (NetworkInterface) nis.nextElement();
				Enumeration<InetAddress> ias = ni.getInetAddresses();
				InetAddress ia = null;

				LOGGER.debug("Varrendo portas da interface "
						+ ni.getDisplayName());

				while (ias.hasMoreElements()) {
					ia = (InetAddress) ias.nextElement();
					varrerPortas(ia);
				}
			}

			try {
				LOGGER.info("Vou aguardar uns 10 segundos antes da próxima varredura");
				Thread.sleep(timeToSleep);
			} catch (InterruptedException e) {
				LOGGER.debug("Não consigo esperar! ", e.getCause());
			}

		}

	}

	/** <b>private void varrerPortas</b><br><br>
	 *
	 * Método privado da Aranha que varre a porta de uma máquina que ela encontrou na rede */
	private void varrerPortas(InetAddress enderecoAlvo) {

		Socket socket = null;

		for(int porta = PORTA_MINIMA; porta <= PORTA_MAXIMA; porta++) {

			try {
				socket = new Socket(enderecoAlvo, porta);
				LOGGER.debug("Socket aberto no host: " + enderecoAlvo.getHostAddress() + ":" + porta);

				Envelope envio = new Envelope(getEndereco().getHostAddress(), getPorta(),
						NOME_AGENTES.ARANHA, MENSAGENS_AGENTES.IDENTIFICAR, null);

				SocketUtils.enviarMensagem(socket, envio);

				try {

					Envelope retorno = SocketUtils.aguardarMensagem(socket);

					/* Esse pacote é gerado para guardar informações corretas do endereço de IP do agente identifcado */
					Envelope correcaoRetorno = new Envelope(socket.getInetAddress().getHostAddress(), socket.getPort(), retorno.getInstanciaAgente(), retorno.getMensagem(), null);

					if(!agentesIdentificados.contains(correcaoRetorno)) {
						agentesIdentificados.add(correcaoRetorno);
					}

				} catch (ClassNotFoundException e) {
					LOGGER.error("Não foi possível identificar o tipo do agente");
				}

			} catch (IOException e) {
			} finally {
				try {
					// Fecha o socket
					if (socket != null)
						socket.close();
				} catch (IOException ex) {
				}
			}
		}
	}

	/** <b>private Envelope devolverAgente</b><br><br>
	 *
	 *	Devolve um agente que a Aranha tem na sua lista... se tiver algum!
	 * */
	private Envelope devolverAgente(NOME_AGENTES instanciaAgente) {
		LOGGER.debug("Iniciando busca pelo agente alvo");

		LOGGER.debug("Varrendo a lista de agentes...");

		int pontoAtual = posicaoLista;
		Envelope agenteTemp;

		if(pontoAtual == 0) { //lista vazia
			LOGGER.info("Oohh... a lista está vazia. Não tenho ninguém para devolver...");
			return buildEnvelope(NOME_AGENTES.ARANHA, MENSAGENS_AGENTES.AGENTE_INDISPONIVEL, null);
		}

		do {
			LOGGER.debug("Colocando um agente na lista para verificação");
			agenteTemp = agentesIdentificados.get(pontoAtual);

			if(instanciaAgente == null || agenteTemp.getInstanciaAgente().equals(instanciaAgente)) {
				LOGGER.debug("Agente encontrado. Entregando agente para solicitante");
				posicaoLista = pontoAtual;
				LOGGER.info("Devolvendo o agente " + agenteTemp.toString());
				return agenteTemp;
			}

			LOGGER.debug("Esse agente não é o desejado! Vamos para o próximo");
			if(pontoAtual == agentesIdentificados.size() - 1) {
				pontoAtual = -1;
			}

			pontoAtual++;
		} while ( pontoAtual != posicaoLista );

		LOGGER.warn("Não tem nenhum agente que eu possa devolver! Vou retornar NULL só para sacanear o desenvolvedor!");
		return null;
	}

}
