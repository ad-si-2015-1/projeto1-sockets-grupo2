package br.ufg.si.ad.grupo2.utils.interpretador.aritmetico;

import java.math.BigDecimal;

import br.ufg.si.ad.grupo2.utils.interpretador.aritmetico.exception.CalculoImpossivelException;

public class Operacao {
	TipoOperacao tipoOperacao;
	BigDecimal param1, param2;
	
	
	public Operacao(TipoOperacao tipoOperacao, BigDecimal param1,
			BigDecimal param2) {
		this.tipoOperacao = tipoOperacao;
		this.param1 = param1;
		this.param2 = param2;
	}


	public Operacao(TipoOperacao tipoOperacao, String param1, String param2) {
		this.tipoOperacao = tipoOperacao;
		this.param1 = BigDecimal.valueOf(Double.valueOf(param1));
		this.param2 = BigDecimal.valueOf(Double.valueOf(param2));
	}

	
	@Override
	public String toString(){
		return param1 + tipoOperacao.getSignal() + param2;
	}


	public BigDecimal calcular() {
		try {
			MathExecutor mathExec = new MathExecutor();
			return (BigDecimal) tipoOperacao.getExecutor().invoke(mathExec, param1, param2);
		} catch (Exception e) {
			throw new CalculoImpossivelException("Erro ao calcular" + toString(), e);
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operacao other = (Operacao) obj;
		if (param1 == null) {
			if (other.param1 != null)
				return false;
		} else if (!param1.equals(other.param1))
			return false;
		if (param2 == null) {
			if (other.param2 != null)
				return false;
		} else if (!param2.equals(other.param2))
			return false;
		if (tipoOperacao != other.tipoOperacao)
			return false;
		return true;
	}
}

