package br.ufg.si.ad.grupo2.modelo;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.utils.Envelope;

/** <b>Classe Agente</b>
 * <br><br>
 * Classe que contém a implementação base de todos os agentes que serão utilizados na aplicação.
 *<br><br>
 * Define as regras de implementação dos agentes que herdam dessa classe.
 *
 * @author Bruno Nogueira*/
public abstract class Agente {

	/** Definição do Range de portas que poderão ser instanciadas a aplicação <br><br><br>
	 * PORTA_MINIMA = 14440;<br>
	 * PORTA_MAXIMA = 14800;*/
	protected static final int PORTA_MINIMA = 14440;
	protected static final int PORTA_MAXIMA = 14800;
	private static final Logger LOGGER = LoggerFactory.getLogger(Agente.class);

	/** <b>private InetAddress endereco</b><br><br>
	 * Contém o endereço de IP da máquina onde o usuário foi instanciado<br><br>
	 * Essa informação é pega do serverSocket;*/
	private InetAddress endereco;

	/** <b>private int porta</b> <br><br>
	 *
	 * Contém o número da porta onde o agente está ouvindo;
	 *
	 * */
	private int porta = PORTA_MINIMA;

	/**<b>private Boolean ativo</b><br><br>
	 *
	 *  Variável booleana que informa se o agente está apto para disparar a thread de ação dele
	 *  <br><br>
	 *  Sempre inicia com @false pois todos os usuários só ativam a thread de ação quando contactados pela Aranha.
	 *  <br><br>
	 *  A única exceção é a Aranha que inicia sua Thread quando é instanciada
	 *  */
	private Boolean ativo = false;

	private String enderecoAranha;
	private int portaAranha;

	private ServerSocket serverSocket = null;

	/** Construtor base de Agente. <br><br>
	 *
	 *  Responsável por instanciar um serverSocket para o agente conseguir escutar a rede;
	 *  <br><br>
	 *
	 *  Cria as informações básicas de localização do agente na rede;
	 *  */
	Agente() {

		while(serverSocket == null && porta != PORTA_MAXIMA) {
			try {
				serverSocket = new ServerSocket(porta);
				LOGGER.debug("Estou ouvindo na porta: " + porta);
			} catch(IOException e) {
				LOGGER.debug("Porta " + porta + " indisponível para uso");
				porta++;
			}
		}

		setEndereco(serverSocket.getInetAddress());

	}

	/** <b>abstract void ouvir</b><br><br>
	 *
	 * Método abstrato que implementa a regra que todo agente deve ouvir alguma conexão <br><br>
	 *
	 * @author Bruno Nogueira
	 * @throws IOException */
	void ouvir() throws IOException {

		while(serverSocket != null) {
			LOGGER.debug("Aguardando conexão do próximo agente...");
			Socket socket = serverSocket.accept();

			acaoEscutarImp(socket);

		}

		serverSocket.close();
	}

	void agir() {

		while(serverSocket != null) {
			LOGGER.debug("Iniciando atividade do agente...");

			if(ativo) {
				acaoAgirImp();

			} else {

				try {
					LOGGER.debug("Não tenho o que fazer, durmo por 5 segundos");
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}

	}

	/** <b>abstract void acaoEscutarImp</b>
	 * <br><br>
	 * Método responsável por implementar qual ação o agente vai executar quando recebe uma conexão */
	abstract void acaoEscutarImp(Socket socket) ;

	/** <b>abstract void agir</b>
	 * <br><br>
	 * Método responsável por implementar qual ação o agente vai executar durante a sua vida */
	abstract void acaoAgirImp() ;

	/* ==================== Métodos Getters e Seeters do Agente ==================== */
	public InetAddress getEndereco() {
		return endereco;
	}
	void setEndereco(InetAddress endereco) {
		this.endereco = endereco;
	}

	public int getPorta() {
		return porta;
	}
	void setPorta(int porta) {
		this.porta = porta;
	}

	public String getEnderecoAranha() {
		return enderecoAranha;
	}
	void setEnderecoAranha(String enderecoAranha) {
		this.enderecoAranha = enderecoAranha;
	}

	public int getPortaAranha() {
		return portaAranha;
	}
	void setPortaAranha(int portaAranha) {
		this.portaAranha = portaAranha;
	}

	Boolean getAtivo() {
		return ativo;
	}
	void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	protected Envelope buildEnvelope(NOME_AGENTES meuTipoAgente, MENSAGENS_AGENTES msg, String msgPersonalizada) {
		return new Envelope(getEndereco().getHostName(), getPorta(), meuTipoAgente, msg, msgPersonalizada);
	}

	/** Método que mantem o endereço da Aranha através do contato do socket; */
	protected void guardarEnderecoAranha(Socket socket, Envelope envelope) {
		LOGGER.debug("Guardando informações da Aranha! IP: " + socket.getInetAddress().getHostAddress() + "Porta: " + envelope.getPorta());
		setEnderecoAranha(socket.getInetAddress().getHostAddress());
		setPorta(envelope.getPorta());
		LOGGER.debug("Informações Armazenadas");
	}

}
