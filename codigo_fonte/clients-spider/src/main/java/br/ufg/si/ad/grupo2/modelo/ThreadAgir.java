package br.ufg.si.ad.grupo2.modelo;

public class ThreadAgir implements Runnable {

	private Agente agente;

	public ThreadAgir(Agente agente) {
		this.agente = agente;
	}

	public void run() {

		agente.agir();

	}

}
