package br.ufg.si.ad.grupo2.modelo;

import java.io.IOException;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.utils.Envelope;
import br.ufg.si.ad.grupo2.utils.SocketUtils;

class Cacador extends Agente {

	private static final Logger LOGGER = LoggerFactory.getLogger(Cacador.class);

    Cacador(){
        super();
        LOGGER.info("Iniciando a caçada aos impuros");
    }


    @Override
    void acaoEscutarImp(Socket socket) {
        Envelope mensagem = null;
		try {
			mensagem = SocketUtils.aguardarMensagem(socket);
			LOGGER.debug("Recebida mensagem {}", mensagem);

			Envelope resposta;

			switch(mensagem.getMensagem()) {

	        case IDENTIFICAR:
	        	LOGGER.debug("Recebendo contato do X9 Aranha!");

	        	guardarEnderecoAranha(socket, mensagem);

	            resposta = buildEnvelope(NOME_AGENTES.CACADOR, MENSAGENS_AGENTES.SOU_O_AGENTE, null);

	            LOGGER.debug("Enviando minha identificação para o X9");
				SocketUtils.enviarMensagem(socket, resposta);

				setAtivo(true);
				break;

	        case MORDER:
	        	LOGGER.debug("Algum insolente está tentando me morder!");
	        	resposta = buildEnvelope(NOME_AGENTES.CACADOR, MENSAGENS_AGENTES.SOU_IMUNE, null);

	        	LOGGER.debug("Ele não conseguiu me contaminar");
	        	break;

			default:
				LOGGER.debug("Comando desconhecido! Ignorando...");
				resposta = buildEnvelope(NOME_AGENTES.CACADOR, MENSAGENS_AGENTES.SOLICITACAO_DESCONHECIDA, null);

				SocketUtils.enviarMensagem(socket, resposta);

	        }
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }


    @Override
    void acaoAgirImp() {
        // TODO Auto-generated method stub

    }


}
