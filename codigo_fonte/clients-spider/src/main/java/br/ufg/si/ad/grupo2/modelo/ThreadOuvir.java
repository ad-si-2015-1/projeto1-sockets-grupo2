package br.ufg.si.ad.grupo2.modelo;

import java.io.IOException;


public class ThreadOuvir implements Runnable {

	private Agente agente;

	public ThreadOuvir(Agente agente) {
		this.agente = agente;
	}

	public void run() {
		try {

			agente.ouvir();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
