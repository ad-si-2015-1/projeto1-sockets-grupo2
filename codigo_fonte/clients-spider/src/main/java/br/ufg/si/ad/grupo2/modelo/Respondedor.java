package br.ufg.si.ad.grupo2.modelo;

import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.utils.Envelope;
import br.ufg.si.ad.grupo2.utils.SocketUtils;
import br.ufg.si.ad.grupo2.utils.interpretador.aritmetico.InterpretadorAritmetico;
import static br.ufg.si.ad.grupo2.modelo.MENSAGENS_AGENTES.*;

@SuppressWarnings("unused")
public class Respondedor extends Agente {


	private static final Logger LOGGER = LoggerFactory.getLogger(Respondedor.class);

	private int acertos = 0;
	private int erros = 0;

	Respondedor() {
		super();
		LOGGER.info("Eu quero é ver quem é o Questionador que vai me falar algo que não sei responder ...");
	}

	@Override
	void acaoEscutarImp(Socket socket) {
		try {
			Envelope incommingMessage = SocketUtils.aguardarMensagem(socket);
			LOGGER.debug("Recebida mensagem {}", incommingMessage);

			MENSAGENS_AGENTES mensagem = incommingMessage.getMensagem();
			switch (mensagem) {
			case IDENTIFICAR:
				LOGGER.info("Recebi um pedido de identicação. Alá salve a Aranha! \\o/");
				identificarSe(socket, incommingMessage);
				break;

			case MORDER:
				LOGGER.info("Um zumbi... MAMÃE!!");

				Envelope resposta = buildEnvelope(NOME_AGENTES.RESPONDEDOR, MENSAGENS_AGENTES.FUI_MORDIDO, null);

				SocketUtils.enviarMensagem(socket, resposta);

				LOGGER.info("Agente infectado");
				AgenteFactory.getAgenteFactory().contaminar(this, NOME_AGENTES.RESPONDEDOR);

			default:
				LOGGER.warn("Nao sei responder a esse comando. Ignorando...");
				SocketUtils.enviarMensagem(socket, mensagemDeIgnorancia());

				break;
			}

		} catch (Exception e) {
			LOGGER.error("Erro ao escutar", e);
		}

	}

	@Override
	void acaoAgirImp() {

		iniciarPedidoDesafio();

		esperar();
	}


	private void identificarSe(Socket socket, Envelope incommingMessage) {
		LOGGER.debug("Guardando endereco da aranha para uso futuro");
		guardarEnderecoAranha(socket, incommingMessage);
		LOGGER.info("Identificando-se para a aranha na rede");
		Envelope identificacao = buildEnvelope(NOME_AGENTES.RESPONDEDOR, MENSAGENS_AGENTES.SOU_O_AGENTE, null);

		try {
			LOGGER.debug("Respondendo a aranha com a mensagem {}", identificacao);
			SocketUtils.enviarMensagem(socket, identificacao);
		} catch (Exception e) {
			LOGGER.error("Erro ao enviar identificacao para a aranha", e);
		}
	}

	private void responderDesafio(Socket socket, Envelope incommingMessage) {
		LOGGER.debug("Iniciando procedimentos de resposta dos desafios");
		String desafio = incommingMessage.getMensagemPersonalizada();
		String resposta = null;
		if (desafio == null) {
			resposta = "é o que, bixo? me desafiou sem enviar desafio decente!???  :C";
		} else {
			resposta = calcularResposta(desafio);
			LOGGER.info("A resposta é: " + resposta);
		}
		Envelope respostaDesafio = buildEnvelope(NOME_AGENTES.RESPONDEDOR, MENSAGENS_AGENTES.MINHA_RESPOSTA, resposta);
		try {
			SocketUtils.enviarMensagem(socket, respostaDesafio);
		} catch (Exception e) {
			LOGGER.error("Erro ao responder a um desafio", e);
		}
	}

	private String calcularResposta(String desafio) {
		String resposta = InterpretadorAritmetico.calcularString(desafio);
		return resposta;
	}

	private void iniciarPedidoDesafio() {
		Envelope questionadorObtido = getQuestionadorFromAranha();

		if (questionadorObtido==null || questionadorObtido.getMensagem() == MENSAGENS_AGENTES.AGENTE_INDISPONIVEL) {
			LOGGER.debug("Não foi possivel obter um Questionador da aranha.");
			return;
		}

		try {
			Socket socketQuestionador = new Socket(questionadorObtido.getIp(), questionadorObtido.getPorta());
			Envelope desafio = pedirDesafio(socketQuestionador);
			if (desafio !=null && desafio.getMensagemPersonalizada() != null) {
				responderDesafio(socketQuestionador, desafio);
			}

			Envelope resultado = SocketUtils.aguardarMensagem(socketQuestionador);

			if(resultado.getMensagem().equals( MENSAGENS_AGENTES.CERTO )) {
				acertos++;
			} else if (resultado.getMensagem().equals( MENSAGENS_AGENTES.ERRADO )) {
				erros++;
			}
		} catch (Exception e) {
			LOGGER.error("Erro ao interagir com um Questionador", e);
		}

	}


	private Envelope pedirDesafio(Socket socketQuestionador) {
		Envelope pedidoRespondedor = buildEnvelope(NOME_AGENTES.RESPONDEDOR, ME_DESAFIE, null);
		try {
			SocketUtils.enviarMensagem(socketQuestionador, pedidoRespondedor);
			Envelope desafioDoQuestionador = SocketUtils.aguardarMensagem(socketQuestionador);
			return desafioDoQuestionador;
		} catch (Exception e) {
			LOGGER.error("Erro ao buscar um questionador da aranha", e);
			return null;
		}
	}


	private Envelope getQuestionadorFromAranha() {
		Envelope pedidoRespondedor = buildEnvelope(NOME_AGENTES.QUESTIONADOR, INFORME_UM_AGENTE, null);
		try {
			Socket socket = new Socket(getEnderecoAranha(), getPortaAranha());
			SocketUtils.enviarMensagem(socket, pedidoRespondedor);
			Envelope respostaAranhaQuestionador = SocketUtils.aguardarMensagem(socket);
			return respostaAranhaQuestionador;
		} catch (Exception e) {
			LOGGER.error("Erro ao buscar um questionador da aranha", e);
			return null;
		}
	}

	private Envelope mensagemDeIgnorancia() {
		return buildEnvelope(NOME_AGENTES.QUESTIONADOR, MENSAGENS_AGENTES.SOLICITACAO_DESCONHECIDA, null);
	}

	private void esperar() {
		try {
			Thread.sleep(5000);
			return;
		} catch (Exception e) {
			LOGGER.error("Nao consegui esperar... coisas estranhas podem acontecer.");
			return;
		}
	}

}
