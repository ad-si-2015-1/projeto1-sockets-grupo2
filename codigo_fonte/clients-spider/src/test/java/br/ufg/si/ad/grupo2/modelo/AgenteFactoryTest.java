package br.ufg.si.ad.grupo2.modelo;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import br.ufg.si.ad.grupo2.modelo.Agente;
import br.ufg.si.ad.grupo2.modelo.AgenteFactory;
import br.ufg.si.ad.grupo2.modelo.Aranha;

/**
 * Classe de teste unitário da classe {@link AgenteFactory}.
 *  
 * @author Ana Letícia
 *
 */
public class AgenteFactoryTest {
	
	AgenteFactory sut;
	
	public AgenteFactoryTest() {
		sut = AgenteFactory.getAgenteFactory();
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o id 0 (zero), a instância retornada é de fato
	 * uma {@link Aranha}.
	 */
	@Test
	public void testCriarAgenteComIdZeroDeveRetornarAranha() {
		Agente agente = sut.criarAgente(0);
		assertTrue(isAranha(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o nome de 'aranha', a instância retornada é de fato
	 * uma {@link Aranha}.
	 */
	@Test
	public void testCriarAgenteComNomeAranhaDeveRetornarAranha() {
		Agente agente = sut.criarAgente("aranha");
		assertTrue(isAranha(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o id 1 (um), a instância retornada é de fato
	 * uma {@link Zumbi}.
	 */
	@Test
	public void testCriarAgenteComIdUmDeveRetornarZumbi() {
		Agente agente = sut.criarAgente(1);
		assertTrue(isZumbi(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o nome de 'zumbi', a instância retornada é de fato
	 * uma {@link Zumbi}.
	 */
	@Test
	public void testCriarAgenteComNomeZumbiDeveRetornarZumbi() {
		Agente agente = sut.criarAgente("zumbi");
		assertTrue(isZumbi(agente));
	}

	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o id 2 (dois), a instância retornada é de fato
	 * uma {@link Respondedor}.
	 */
	@Test
	public void testCriarAgenteComIdDoisDeveRetornarRespondedor() {
		Agente agente = sut.criarAgente(2);
		assertTrue(isRespondedor(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o nome de 'respondedor', a instância retornada é de fato
	 * uma {@link Respondedor}.
	 */
	@Test
	public void testCriarAgenteComNomeRespondedorDeveRetornarRespondedor() {
		Agente agente = sut.criarAgente("respondedor");
		assertTrue(isRespondedor(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o id 3 (tres), a instância retornada é de fato
	 * uma {@link Questionador}.
	 */
	@Test
	public void testCriarAgenteComIdTresDeveRetornarQuestionador() {
		Agente agente = sut.criarAgente(3);
		assertTrue(isQuestionador(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o nome de 'questionador', a instância retornada é de fato
	 * uma {@link Questionador}.
	 */
	@Test
	public void testCriarAgenteComNomeQuestionadorDeveRetornarQuestionador() {
		Agente agente = sut.criarAgente("questionador");
		assertTrue(isQuestionador(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o id 4 (quatro), a instância retornada é de fato
	 * uma {@link Cacador}.
	 */
	@Test
	public void testCriarAgenteComIdQuatroDeveRetornarCacador() {
		Agente agente = sut.criarAgente(4);
		assertTrue(isCacador(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o nome de 'cacador', a instância retornada é de fato
	 * uma {@link Cacador}.
	 */
	@Test
	public void testCriarAgenteComNomeCacadorDeveRetornarCacador() {
		Agente agente = sut.criarAgente("cacador");
		assertTrue(isCacador(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o id 5 (cinco), a instância retornada é de fato
	 * uma {@link Curador}.
	 */
	@Test
	public void testCriarAgenteComIdCincoDeveRetornarCurador() {
		Agente agente = sut.criarAgente(5);
		assertTrue(isCurador(agente));
	}
	
	/**
	 * Checa se ao solicitar a construção de um {@link Agente}
	 * com o nome de 'curador', a instância retornada é de fato
	 * uma {@link Curador}.
	 */
	@Test
	public void testCriarAgenteComNomeCuradorDeveRetornarCurador() {
		Agente agente = sut.criarAgente("curador");
		assertTrue(isCurador(agente));
	}
	
	private boolean isAranha(Agente agent) {
		return agent instanceof Aranha;
	}

	private boolean isZumbi(Agente agent) {
		return agent instanceof Zumbi;
	}
	
	private boolean isRespondedor(Agente agente) {
		return agente instanceof Respondedor;
	}
	
	private boolean isQuestionador(Agente agente) {
		return agente instanceof Questionador;
	}
	
	private boolean isCacador(Agente agent) {
		return agent instanceof Cacador;
	}
	
	private boolean isCurador(Agente agente) {
		return agente instanceof Curador;
	}
	
	
}
