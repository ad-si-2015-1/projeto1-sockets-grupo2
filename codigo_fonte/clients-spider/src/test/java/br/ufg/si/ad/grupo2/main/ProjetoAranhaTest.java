package br.ufg.si.ad.grupo2.main;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.grupo2.modelo.Agente;
import br.ufg.si.ad.grupo2.modelo.AgenteFactory;
import br.ufg.si.ad.grupo2.modelo.Aranha;
import br.ufg.si.ad.grupo2.modelo.ThreadAgir;
import br.ufg.si.ad.grupo2.modelo.ThreadOuvir;

/**
 * Teste que simula a execução do jogo, sendo que é instanciada uma única
 * {@link Aranha} e vários {@link Agente}s aleatórios.
 * 
 * @author Ana Letícia
 * 
 */
public class ProjetoAranhaTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProjetoAranhaTest.class);

	private Aranha aranha;
	private Agente[] agentes = new Agente[5];
	
	@Test
	public void testProjetoAranha() {
		iniciarAranha();
		
		iniciarAgentesAleatorios();
		
		while(true);
	}

	/**
	 * Inicia um agente do tipo Aranha.
	 */
	private void iniciarAranha() {
		LOGGER.info("Iniciando aranha...");
		
		aranha = (Aranha) AgenteFactory.getAgenteFactory().criarAgente("aranha");
		new Thread(new ThreadOuvir(aranha)).start();
		new Thread(new ThreadAgir(aranha)).start();
		
		LOGGER.info("Aranha iniciado...");
	}
	
	/**
	 * Inicia vários agentes aleatórios.<br>
	 * Em seguida instancia uma {@link Thread} para {@link ThreadOuvir} e outra para {@link ThreadAgir}.
	 * 
	 */
	private void iniciarAgentesAleatorios() {
		LOGGER.info("Iniciando agentes aleatorios...");
		
		for (int i = 0; i < agentes.length; i++) {
			agentes[i] = criarAgenteAleatorio();

			/* Iniciando Threads de escuta */
			LOGGER.debug("Instanciando Threads de escuta e execução");

			new Thread(new ThreadOuvir(agentes[i])).start();
			new Thread(new ThreadAgir(agentes[i])).start();

			LOGGER.debug("O agente " + agentes[i].getClass().getName()
					+ " foi instanciado no IP "
					+ agentes[i].getEndereco().getHostAddress() + " na porta "
					+ agentes[i].getPorta());
		}
		
		LOGGER.info("Agentes aleatorios iniciados...");
	}

	/**
	 * @return apenas retorna um {@link Agente} qualquer.
	 */
	private Agente criarAgenteAleatorio() {
		return AgenteFactory.getAgenteFactory().criarAgente();
	}

}
